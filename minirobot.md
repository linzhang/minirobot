MiniRobot Manual
====

##General

The miniRobot is made by plastic plates which were cut by laser machines in FabLab Leuven. 

There are five joints on the robot, joint 1 to 5, counting from the base to the end effector. 
Seven servo motors are installed on the robot to keep a stable mechanical structure and to provide sufficient torque to move. Each of the motor is labeled with a number, from 1 to 7. However, motor #7 is **not** powered but used as an mechanical stablizer. 

The second joint is driven by motor 2 and motor 6, which are installed back to back. Therefore, motor 2 and motor 6 should turn the **same angle** but to **oppose direction** to drive the second joint.

##Hardware

###Servo Motor
The servo motors are powered by 4.8V-6.0V DC voltage, each consuming about 200mA without load. 

The servo motors are controlled by PWM signals with 20ms period. They are position controlled, the absolute position is linearly proportional to the PWM duty cycle. The servo motors rotate in (-90, 90) degrees, corresponding to (542, 2342) microseconds PWM duty cycle. The motor stays at origin (0 degree) at 1442 us PWM duty cycle. 

###Robot Specification
The robot is 33.7 cm long when fully stretched, measuring from the middle layer of the base.

![Pins][1]
[1]: https://bitbucket.org/linzhang/minirobot/raw/master/fig/home.png

###PWM generator co-processor
Each PWM generator co-processor needs two parameters:

* 1) PWM period
* 2) PWM pulse width (duty cycle)

The above parameters are set through registers mapped at **0x43C00000**:

	int *pt;
	Motor1 PWM period: 0x43C00000,  *(pt+0)
	Motor1 PWM pulse : 0x43C00004,  *(pt+1)
	
	Motor2 PWM period: 0x43C00008,  *(pt+2)
	Motor2 PWM pulse : 0x43C0000C,  *(pt+3)
	
	Motor3 PWM period: 0x43C00010,  *(pt+4)
	Motor3 PWM pulse : 0x43C00014,  *(pt+5)
	
	Motor4 PWM period: 0x43C00018,  *(pt+6)
	Motor4 PWM pulse : 0x43C0001C,  *(pt+7)
	
	Motor5 PWM period: 0x43C00020,  *(pt+8)
	Motor5 PWM pulse : 0x43C00024,  *(pt+9)
	
	Motor6 PWM period: 0x43C00028,  *(pt+10)
	Motor6 PWM pulse : 0x43C0002C,  *(pt+11)

###FPGA Configuration
Make sure that jumpers JP2 and JP3 are CLOSED, the configuration jumpers JP11 to JP7 are set to Low-High-High-Low-Low for SD boot up.
Insert SD card Z-2 into the SD card slot and switch on the board, LED LD12 “DONE” is on when the boot is done, Linux kernel is loaded afterwards.

###PWM Outputs
The six PWM signals for motor 1 to 6 are output on JA1 connector, at pin 1,2,3,4,7 and 8. 

###MiniRobot power board
The FPGA board itself is not capable to drive all six motors, an external 5V/4A power supply is needed. Pin 5 of JA1 should be connected to the ground pin on the power board.

![Pins][2]
[2]: https://bitbucket.org/linzhang/minirobot/raw/master/fig/connector_pin.png



Software
====

###IP Address
SD card Z-2 is 192.168.10.168. The backup SD card Z-1 has IP address 192.168.10.108.

###Embedded Linux
Use **SSH** to login to the FPGA, both username and password are “linaro”. 


	$ ssh linaro@192.168.10.168
	
The examples are in /home/linaro/work/all-pro

Joint space control 
	
	$ cd /home/linaro/work/all-pro/joint_servo
	$ ./configure
	$ make
	$ sudo ./servo 0 0 0 0 0

Cartesian space control

	$ cd /home/linaro/work/all-pro/servo
	$ ./configure
	$ make
	$ sudo ./servo 0 0 0.337 1000

The first run takes about 20 seconds, the same PWM signals are sent to 6 LEDs on the board for visualization. 

###Memory Map
Six PWM generators are implemented on the FPGA. They are accessible by mmap in C/C++ code:

	#define PWM_GEN_FPGA_ADDR 0x43C00000
	int fd;
	int *pt; //pointer for PWM generators.
	fd = open("/dev/mem", O_RDWR);
	pt = (int *)mmap(0, 256, PROT_READ|PROT_WRITE, MAP_SHARED, fd, PWM_GEN_FPGA_ADDR);


###PWM initialization and Robot Joints
The PWM generators are initialized by setting PWM period registers *pt, *(pt+2), *(pt+4), *(pt+6), *(pt+8), *(pt+10).
	
	#define NUM_MOTORS 6
	void setPwmPeriod(int *pt, int totalNanoSec){
        for (int i=0;i<NUM_MOTORS;i++){
                *(pt+i*2)=totalNanoSec;
        }
	}

Setting 1442us duty cycle PWM to all motors would drive the robot to the arbitrary home position, fully stretched. The conversion between radians and PWM are written in functions in rad2pwm.cpp in the Cartesian space example:

	#define PI 3.1415926
	#define HOME_POS_PWM 1442 //microsecond
	#define RANGE 900 //microsecond
	#define EQV_MICRO_SEC 100 //On the FPGA, 100 clock period = 1us.
	#define LOWER_LIMIT (HOME_POS_PWM-RANGE)*EQV_MICRO_SEC
	#define UPPER_LIMIT (HOME_POS_PWM+RANGE)*EQV_MICRO_SEC

	void setJointPositions(int * pt, float desired_pos[5]){
		*(pt+1)=(int)((desired_pos[0]+PI/2)/PI*(UPPER_LIMIT-LOWER_LIMIT))+LOWER_LIMIT; //set position for motor 1 (base)
		*(pt+3)=(int)((desired_pos[1]+PI/2)/PI*(UPPER_LIMIT-LOWER_LIMIT))+LOWER_LIMIT; //set position for motor 2
		*(pt+5)=(int)((desired_pos[2]+PI/2)/PI*(UPPER_LIMIT-LOWER_LIMIT))+LOWER_LIMIT; //set position for motor 3
		*(pt+7)=(int)((desired_pos[3]+PI/2)/PI*(UPPER_LIMIT-LOWER_LIMIT))+LOWER_LIMIT; //set position for motor 4
		*(pt+9)=(int)((desired_pos[4]+PI/2)/PI*(UPPER_LIMIT-LOWER_LIMIT))+LOWER_LIMIT; //set position for motor 5 (end effector)
		*(pt+11)=UPPER_LIMIT - (int)((desired_pos[1]+PI/2)/PI*(UPPER_LIMIT-LOWER_LIMIT)); //set position for motor 6
}

The current joint positions are read by:

	void getJointPositions(int * pt, float current_pos[5]){
		current_pos[0]=(float)(*(pt+1)-LOWER_LIMIT)/(UPPER_LIMIT-LOWER_LIMIT)*PI-PI/2;
		current_pos[1]=(float)(*(pt+3)-LOWER_LIMIT)/(UPPER_LIMIT-LOWER_LIMIT)*PI-PI/2;
		current_pos[2]=(float)(*(pt+5)-LOWER_LIMIT)/(UPPER_LIMIT-LOWER_LIMIT)*PI-PI/2;
		current_pos[3]=(float)(*(pt+7)-LOWER_LIMIT)/(UPPER_LIMIT-LOWER_LIMIT)*PI-PI/2;
		current_pos[4]=(float)(*(pt+9)-LOWER_LIMIT)/(UPPER_LIMIT-LOWER_LIMIT)*PI-PI/2;
	};
